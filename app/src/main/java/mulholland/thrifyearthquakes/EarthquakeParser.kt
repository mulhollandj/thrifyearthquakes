package mulholland.thrifyearthquakes

import android.util.JsonReader
import java.io.InputStream
import java.io.InputStreamReader

class EarthquakeParser {
    companion object {
        fun parse(earthquakeData: InputStream): List<EarthquakeData> {
            val datas = mutableListOf<EarthquakeData>()
            val reader = JsonReader(InputStreamReader(earthquakeData))
            reader.beginObject()
            reader.nextName()
            reader.beginArray()
            while (reader.hasNext()) {
                datas.add(readEarthquakeData(reader))
            }
            reader.endArray()
            reader.endObject()
            return datas
        }

        private fun readEarthquakeData(reader: JsonReader): EarthquakeData {
            var magnitude = 0.0
            var longitude = 0.0
            var latitude = 0.0
            reader.beginObject()
            while (reader.hasNext()) {
                val name = reader.nextName()
                when (name) {
                    "magnitude" -> magnitude = reader.nextDouble()
                    "lng" -> longitude = reader.nextDouble()
                    "lat" -> latitude = reader.nextDouble()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return EarthquakeData(magnitude, latitude, longitude)
        }
    }
}
