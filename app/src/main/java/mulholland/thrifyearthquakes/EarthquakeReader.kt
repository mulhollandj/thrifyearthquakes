package mulholland.thrifyearthquakes

import android.os.AsyncTask
import android.util.Log
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

const val CONNECT_SUCCESS = 0
const val GET_INPUT_STREAM_SUCCESS = 1

class EarthquakeReader {
    companion object {
        const val READ_TIMEOUT = 3000
        const val CONNECT_TIMEOUT = 3000
    }
    private lateinit var mCallback: EarthquakeDataCallback

    private var mDownloading: Boolean = false

    fun loadEarthquakes(callback: EarthquakeDataCallback) {
        mCallback = callback
        startDownload()
        mDownloading = true
    }

    private var mDownloadTask: LoadEarthQuakeDataAsyncTask? = null

    private fun startDownload() {
        cancelDownload()
        mCallback.also { callback ->
            mDownloadTask = LoadEarthQuakeDataAsyncTask(callback).apply {
                execute()
            }
        }
    }

    private fun cancelDownload() {
        mDownloadTask?.cancel(true)
    }

    class LoadEarthQuakeDataAsyncTask(var callback: EarthquakeDataCallback) :
        AsyncTask<String, Int, List<EarthquakeData>>() {
        override fun doInBackground(vararg p0: String?): List<EarthquakeData>? {
            val urlBase = "http://api.geonames.org/earthquakesJSON"
            val urlParams = "?formatted=true&north=44.1&south=-9.9&east=-22.4&west=55.2&username=mkoppelman"
            var earthquakes: List<EarthquakeData> = emptyList()

            if (!isCancelled) {
                earthquakes = try {
                    downloadUrl(URL(urlBase + urlParams)) ?: throw IOException("No response received.")
                } catch (e: IOException) {
                    emptyList()
                }
            }
            return earthquakes
        }

        override fun onPostExecute(result: List<EarthquakeData>?) {
            callback.apply {
                result?.also {
                    if (result.isEmpty()) {
                        error("Unable to retrieve data")
                    } else {
                        dataLoaded(result)
                    }
                    return
                }

                finishDownloading()
            }
        }

        private fun parseResult(resultValue: InputStream): List<EarthquakeData> {
            return EarthquakeParser.parse(resultValue)
        }

        @Throws(IOException::class)
        private fun downloadUrl(url: URL): List<EarthquakeData>? {
            var connection: HttpURLConnection? = null
            return try {
                connection = (url.openConnection() as? HttpURLConnection)
                connection?.run {
                    setDefaults()
                    connect()
                    publishProgress(CONNECT_SUCCESS)
                    if (responseCode != HttpsURLConnection.HTTP_OK) {
                        throw IOException("HTTP error code: $responseCode")
                    }
                    publishProgress(GET_INPUT_STREAM_SUCCESS, 0)
                    inputStream?.let { stream ->
                        parseResult(stream)
                    }
                }
            } catch (e: IOException) {
                Log.e("DOWNLOAD_ERROR", e.toString())
                throw e
            } finally {
                closeConnection(connection)
            }
        }

        private fun HttpURLConnection.setDefaults() {
            readTimeout = READ_TIMEOUT
            connectTimeout = CONNECT_TIMEOUT
            requestMethod = "GET"
            doInput = true
        }

        private fun closeConnection(connection: HttpURLConnection?) {
            connection?.inputStream?.close()
            connection?.disconnect()
        }
    }
}
