package mulholland.thrifyearthquakes

class ViewEarthquakesPresenter(private var viewInterface: EarthQuakesView) : EarthquakeDataCallback {
    interface EarthQuakesView {
        fun updateData(earthquakes: List<EarthquakeData>)
        fun displayError(error: String)
    }

    companion object {
        const val SIGNIFICANT_MAGNITUDE = 8
    }

    private lateinit var mEarthquakes: List<EarthquakeData>
    private val reader = EarthquakeReader()

    override fun dataLoaded(data: List<EarthquakeData>) {
        mEarthquakes = data
        viewInterface.updateData(data)
    }

    override fun error(error: String) {
        viewInterface.displayError(error)
    }

    override fun finishDownloading() {
    }

    fun loadEarthquakeData() {
        reader.loadEarthquakes(this)
    }

    fun isSpecial(earthquake: EarthquakeData): Boolean {
        return earthquake.magnitude >= SIGNIFICANT_MAGNITUDE
    }
}

interface EarthquakeDataCallback {
    fun dataLoaded(data: List<EarthquakeData>)
    fun error(error: String)
    fun finishDownloading()
}

class EarthquakeData(val magnitude: Double, val latitude: Double, val longitude: Double)
