package mulholland.thrifyearthquakes

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import kotlinx.android.synthetic.main.activity_view_earthquakes.*
import kotlinx.android.synthetic.main.content_view_earthquakes.*
import kotlinx.android.synthetic.main.earthquake_layout.view.*

class ViewEarthquakesActivity : AppCompatActivity(), ViewEarthquakesPresenter.EarthQuakesView {
    private val mPresenter: ViewEarthquakesPresenter = ViewEarthquakesPresenter(this)
    private var earthquakes: MutableList<EarthquakeData> = mutableListOf()

    override fun displayError(error: String) {
        errorDisplay.visibility = View.VISIBLE
        earthquakeListView.visibility = View.GONE

        errorDisplay.text = error
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_earthquakes)
        setSupportActionBar(toolbar)
        mPresenter.loadEarthquakeData()

        val adapter = EarthquakeAdapter(baseContext, earthquakes, mPresenter)

        earthquakeListView.layoutManager = getEarthquakeListViewLayoutManager()
        earthquakeListView.adapter = adapter
    }

    private fun getEarthquakeListViewLayoutManager(): RecyclerView.LayoutManager? {
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        return layoutManager
    }

    override fun updateData(earthquakes: List<EarthquakeData>) {
        this.earthquakes.addAll(earthquakes)

        earthquakeListView.visibility = View.VISIBLE
        errorDisplay.visibility = View.GONE

        earthquakeListView.adapter?.notifyDataSetChanged()
    }

    class EarthquakeAdapter(
        private val context: Context?,
        private val earthquakes: List<EarthquakeData>,
        private val presenter: ViewEarthquakesPresenter
    ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        companion object {
            private const val TYPE_HEADER = 0
            private const val TYPE_ITEM = 1
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            if (viewType == TYPE_ITEM) {
                val view = LayoutInflater.from(context).inflate(R.layout.earthquake_layout, parent, false)
                return EarthquakeViewHolder(view)
            } else if (viewType == TYPE_HEADER) {
                val view = LayoutInflater.from(context).inflate(R.layout.earthquake_header_layout, parent, false)
                return EarthquakeHeaderViewHolder(view)
            }

            throw IllegalArgumentException("$viewType + isn't a valid type")
        }

        override fun getItemCount(): Int {
            return earthquakes.size + 1
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is EarthquakeViewHolder) {
                val earthquake = earthquakes[position - 1]
                if (presenter.isSpecial(earthquake)) {
                    holder.magnitude.setTypeface(null, Typeface.BOLD)
                    holder.latitude.setTypeface(null, Typeface.BOLD)
                    holder.longitude.setTypeface(null, Typeface.BOLD)
                } else {
                    holder.magnitude.typeface = Typeface.DEFAULT
                    holder.latitude.typeface = Typeface.DEFAULT
                    holder.longitude.typeface = Typeface.DEFAULT
                }
                holder.magnitude.text = earthquake.magnitude.toString()
                holder.latitude.text = earthquake.latitude.toString()
                holder.longitude.text = earthquake.longitude.toString()
            }
        }

        override fun getItemViewType(position: Int): Int {
            return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
        }

        private fun isPositionHeader(position: Int): Boolean {
            return position == 0
        }
    }

    class EarthquakeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val magnitude: TextView = itemView.magnitudeView
        val latitude: TextView = itemView.latitudeView
        val longitude: TextView = itemView.longitudeView
    }

    class EarthquakeHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
