package mulholland.thrifyearthquakes

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class ViewEarthquakesPresenterTest {

    private val boundary = 8.0

    @Test
    fun isSpecialTrue() {
        val presenter = ViewEarthquakesPresenter(TestViewInterface())
        val specialEarthquake = EarthquakeData(boundary + 1, 0.0, 0.0)
        assertTrue(presenter.isSpecial(specialEarthquake))
    }
    @Test
    fun isSpecialTrueBoundary() {
        val presenter = ViewEarthquakesPresenter(TestViewInterface())
        val specialEarthquake = EarthquakeData(boundary, 0.0, 0.0)
        assertTrue(presenter.isSpecial(specialEarthquake))
    }

    @Test
    fun isSpecialFalse() {
        val presenter = ViewEarthquakesPresenter(TestViewInterface())
        val notSpecialEarthquake = EarthquakeData(boundary - 1, 0.0, 0.0)
        assertFalse(presenter.isSpecial(notSpecialEarthquake))
    }

    @Test
    fun isSpecialFalseBoundary() {
        val presenter = ViewEarthquakesPresenter(TestViewInterface())
        val notSpecialEarthquake = EarthquakeData(boundary - .01, 0.0, 0.0)
        assertFalse(presenter.isSpecial(notSpecialEarthquake))
    }
}

class TestViewInterface : ViewEarthquakesPresenter.EarthQuakesView {
    override fun updateData(earthquakes: List<EarthquakeData>) {
    }

    override fun displayError(error: String) {
    }
}