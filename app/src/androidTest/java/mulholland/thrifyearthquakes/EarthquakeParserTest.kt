package mulholland.thrifyearthquakes

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Assert.assertEquals

import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EarthquakeParserTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("mulholland.thrifyearthquakes", appContext.packageName)
    }

    @Test
    fun loadOneDataPoint() {
        val jsonData = getOneDataJson()

        val result = EarthquakeParser.parse(jsonData.byteInputStream())
        assertEquals(result.size, 1)
    }

    @Test
    fun checkFirstDataPoint() {
        val jsonData = getOneDataJson()

        val result = EarthquakeParser.parse(jsonData.byteInputStream())
        assertEquals(result[0].magnitude, 8.8, .001)
        assertEquals(result[0].latitude, 38.322, .001)
        assertEquals(result[0].longitude, 142.369, .001)
    }

    private fun getOneDataJson(): String {
        return "{\"earthquakes\": [\n" +
                "    {\n" +
                "        \"datetime\": \"2011-03-11 04:46:23\",\n" +
                "        \"depth\": 24.4,\n" +
                "        \"lng\": 142.369,\n" +
                "        \"src\": \"us\",\n" +
                "        \"eqid\": \"c0001xgp\",\n" +
                "        \"magnitude\": 8.8,\n" +
                "        \"lat\": 38.322\n" +
                "    }]}"
    }
}